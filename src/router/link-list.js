export const linksList = [{
        title: 'Docs',
        caption: 'quasar.dev',
        icon: 'las la-file-alt',
        link: 'https://quasar.dev'
    },
    {
        title: 'Typography',
        caption: 'Tipografía',
        icon: 'lab la-github',
        link: 'typography'
    },
    {
        title: 'Flex',
        caption: 'Flex',
        icon: 'lab la-github',
        link: 'flex'
    },
    {
        title: 'Dialogs',
        caption: 'Dialogs plugin quasar',
        icon: 'las la-window-minimize',
        link: 'dialogs'
    },
    {
        title: 'Forms',
        caption: 'Forms quasar',
        icon: 'lab la-wpforms',
        link: 'forms'
    },

];